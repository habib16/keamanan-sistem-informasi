from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174095.models import User
# Create your views here.
def index(request):
     return render(request, 'ujian_aplikasi_1174095/index_1174095.html')
def users(request):
     sdata = User.objects.all()
     data = {
         'hasil': sdata,
     }
     return render(request, 'ujian_aplikasi_1174095/users_1174095.html', context=data)