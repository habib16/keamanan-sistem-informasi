from django.db import models

# Create your models here.
class User (models.Model):
    firstname = models.CharField(max_length=20)
    lastname = models.CharField(max_length=21)
    email = models.EmailField(max_length=30,unique=True)

    def __str__(self):
        return "{}".format(self.firstname)
    