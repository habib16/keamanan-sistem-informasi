import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_praktikum_1174027.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174027.models import User
from faker import Faker

fakegen = Faker()
ndepan = ['damara','dwiy','dwis','evie','sri']

def populate(N=30):
    for entry in range(0,N):
        fakelast = fakegen.last_name()
        fakemail = fakegen.email()

        nakhir = User.objects.get_or_create(firstname=random.choice(ndepan),lastname=fakelast,usermail=fakemail)[0]

if __name__ == '__main__':
    inputan = int(input("Masukan Angka = "))
    print("Tunggu Beberapa Saat ...")
    populate(inputan)
    print("data berhasil diisi")